import pandas as pd
import matplotlib.pyplot as plt

# df=pd.read_excel takes the code that is in pandas named .read_excel and reads the data.xlsx file given to it. 
df=pd.read_excel("data.xlsx", skiprows=1)

# (df.head()) basically takes the first five data points so that on your screen you don't get thousands of data printed
print(df.head()) # The command df.head prints first five lines of data frame. a data frame is a special thing with panda

df.plot()
plt.show()

#Want to get at data from df, for particular data sets
#For example: 
#print(df['Binding energy (ev).1'])

# giving values to both x and y in the plots and assign corresponding binding energy/ intensity to them. 
x1 = 	df['Binding energy (eV)']
y1 = 	df['Intensity (counts/second)']

#identifying the max value

first = max(y1)
index = 0
for i in range(len(y1)):
	if first == y1[i]:
		index = i
		break
plt.scatter(x1[index],first, color='red')
plt.show()

plt.clf()
plt.plot(x1,y1)
plt.show(first)


x2 =	df['Binding energy (eV).1']
y2 =	df['Intensity (counts/second).1']

#trying to repeat the code for the second max value, but I'm having an error 

Second = max(y2)
index = 0
for i in range(len(y2)):
        if second == y2[i]:
                index = i
                break
plt.scatter(x2[index],second, color='red')
plt.show()


plt.clf()
plt.plot(x2,y2)
plt.show(second)

x3 =	df['Binding energy (eV).2']
y3 =	df['Intensity (counts/second).2']
plt.clf()
plt.plot(x3,y3)
plt.show()

Third = max(y3)
index = 0
for i in range(len(y3)):
        if second == y3[i]:
                index = i
                break
plt.scatter(x2[index],third,  color='red')
plt.show()

print("Max of list is {}".format(max(y1)))
print("Max of list is {}".format(max(y2)))
print("Max of list is {}".format(max(y3)))
