o 
##Ali Mustafa

1- col_names=["Example 1"," Example 2"]
	this Python command I identify the column or the strings that would be  	imported to our data. As to categorize the xls. Sheet that in use.
2- integrate((z[1]+z[0]*x),(x,2,10)
	this python code helping me to do any Mathmatical operation through symp        y where we can run any expresion. 
3- sympy 
	Sympy provides functionality for symbolic representation of mathematical        ideas
4- import pandas
        This imports pandas into the python working place. Pandas is used to pro        cess data from things like Excel files.

5- Import matplotlib.pyplot as pl
         matplotlib.pyplot is a collection of command style functions that make           matplotlib work like MATLAB. Each pyplot function makes some change to         a figure: e.g., creates a figure, creates a plotting area in a figure,          plots some lines in a plotting area, decorates the plot with labels, et         c.
6- import xlrd is a library for reading data and formatting information from Exc         el files, whether they are .xls or .xlsx files.
7- Print () 
         displays whatever is in the () on screen as text.
8-mport globe Return a possibly-empty list of path names that match pathname, wh         ich must be a string containing a path specification. pathname can be e         ither absolute (like /usr/src/Python-1.5/Makefile) or relative (like ..         /../Tools/*/*.gif), and can contain shell-style wildcards. Broken symli         nks are included in the results (as in the shell).
9- Plotp[] Make plots of DataFrame using matplotlib / pylab.
10- max(value) : I used this code to find the maximume value for the intensity list (y list)  
=======

##Nathan Staley
import numpy as np: this line imports numpy, which works with sets of numbers as np, which makes it easier and quicker to type.
print: this prints the inside data in a file

row_names=["Ex1", "Ex2"]: this code identifies the names of the rows in an excel file.
Importing Pandas allows for the code to create and use the graph of the excel sheet data.

Numpy can be used to run peak detection. THe function used is Array. It is able to work when you set it to find the places where the 
two arrys are highest.

I found out that the best setting to find the peaks in our program was above 10000 in intensity. Anything below that and the data will show many 
more peaks than actually are important to the project.

[MAXTAB, MINTAB] this is the maximum and minimum of the fuction that is used to find the peaks and this is also the parameters for the peaks. the pair will be replaced with the 
x values in the data file.

Graphing. This project requires the data to be graphed and anlysised. That mean that a graph has to be created. If the program just said print, then 
a huge spreadsheet of information would pop up in a text box. That is solved by using the matplot library to plot out the data on a graph that the programer
can set to their own preferances.

ls- this is a mos useful command as it makes it so that the files in the currect directory you are in are displayed in your screen and often acts as a sort of 
GPS. It is able to make it so that one does not have to go back and sort through hundreds of files that are currently running or going to be running.
If I where in the main folder for the teams, I would be able find out exactl where I was and how to extract the nessesary files.

cd- cd is another one of my favorite part of this project, the ablility allows for the user to go back to the beginning commit that started the commit line.
ble

Git Pull. git pulls are also important. During this project, insufficent git pulls compound into problems that cause entire data sets to be erased and a week of work gone because of a mereger. Commiting changes allows for the 
pulls to be attached to the main branch so that the branches of the project do not get off track.

Running your code to make sure it works is also very helpful. Python.whatever file mind.py. that allows for a python script to be run with out risking the code being damaged. 
========
##Laura Denney
1.  (df.data.shift(1) < df.data) & (df.data.shift(-1) < df.data) is saying to look at the peak before and after and if it's smaller than you've
    found a local max
2.  for Sheet in [u'Problem1a', u'Problem1b', u'Problem2']:for...in..[....]:  is a loop that says when you call sheet, you want to 
loop through all of the [..]:.
3.  if indented under the for loop.  you can place an if statement in a for
loop and get all the if conditions to run on all the looped content.
4.  maxtab, mintab = peakdet(y,10000) has python only mark the peaks that are
above 10000.
5.  if len(v) !=len(x): is stating that if the value for the first column is
not equal to the value in the second column, generate an error.
5.  if not isscalar(delta):  states that if the value of delta in not scalar
(it's a vector) than there will be an error.
6.  if delta <=0: states that if delta is less than zero, than there is an error7.  df = Project3.partse(Sheet, skiprows=1) tells python to skip the first row
in Sheet.
8.  Project3 = pd.ExcelFile('P3data.xlsx') is naming the data in Libre Office 
9.  scatterscatter(array(mintab)[:,0], array(mintab)[:,1], color='blue'):  
has python draw the graph in blue
10.  scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='red'):  Has 
python mark the peaks in red. 

=======
##Henri Kunold
1.plt.scatter(thing['x1'], thing['y1'])
 
	The line of code plots data from the array stated above. The "thing" calls for the column of 
	data that can be found in an excel file. More "things" mean different and more columns of data. 

2.plt.show()

	This short line of code is usually written at the end of a set of code. This line creates an 
	image of the plot. Just showing what was previously stated in the code. 

3.import peakutils
	
	This imports a tool that helps with identify and labeling our desired peaks on a plot. Hopefully
	we can work with it to also label the peaks

4.data=numpy.loadtxt('filename')

	This code is used to define code/data using numpy. This is essential for the computer to read 
	and analyze the text.

5.plt.title('graph 1',color ="blue")
	
	This code tells python your desired name of a graph. Likewise, you can add additional design 
	elements such as text color to the graph. In this case I have it set to blue.

6.data=mp.load.txt() 

	This line of code tells python how to define a certain variable. Without defining your variable 
	python will fail to properly run or even run your code at all. 

7.plt.savefig('2') 

	This code is very useful as it saves the ploted figure. This is helpful as you can
	work on the code while viewing the graph. Makes it easy to cross check code and other 
	information.

8.plt.xlabel ('energy')

	This code places a desired function label for the x-axis of a plt. In this case, the x-axis will 	be labeled as 'energy'.

9.plt.ylabel ('intensity') 

	Just like the xlabel code this code places the desried function label for the y-axis plt. In 
	this case, 'intesity'.

10.plt.clt()

	This code clears the figure that was presented by python. It does not close the figure, but 
	allows the user to pull up additional plots. 


##**Crystal Black**
* **print(text)** - using the print command will display any the word(s) within the parenthesis and quotes, without the quotes it will show variables or results. 
* **from peakdet import peakdet** - using this command will look for a file/module named peakdet and will import the content of the file/modue to use in the code of a different file. 
* **import matplotlib.pyplot as p** - this command imports matplotlib.pyplot to be able to utilize the functions within the program, the "as p" part of the command defines the program as "p" so we don�t have to write out matplotlib.pyplot every time.
* **Project3 = pd.ExcelFile("P3data.xlsx")** - this command uses Pandas to import Excel File P3.data.xlsx and makes the data available to use within the code.
* **for Sheet in [u’Problem1a’, u’Problem1b’, u’Problem2’]:** - this command defines Sheet into three different sheets within the P3.data.xlsx file. 
* **df = Project3.parse(Sheet, skiprows=1)** - .parse commands reads the sheets separately so we were able to make 3 different graphs without having 3 different excel files. 
* **skiprows=1** - this command skips 1 row in the excel sheet when it gets used within the code, use it when you dontt want the headers/labels to be ran with the code and only want data without having to change the excel sheet. 
* ** p.savefig(files)** - p.savefig command saves the figures in bitbucket and on your computer saving each figure individually by its previously defined named by using (files) function.
* **for files in filenames:** - the For loop command will repeat the sequence for all elements of data that is defined and run it through the code within the loop and output data for each element. 
* **df.to_excel(files+".xlsx")** - this command saves the information as excel files and names them as the defined file names with .xlsx at the end so you are able to open them in excel. 


