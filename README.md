# p3-command_crew

##Items you need to be able to run the code: 
 - import sys
 - import numpy
 - from numpy import NaN, Inf, arange, isscalar, asarray, array 
 - import glob
 - import matplotlib.pyplot as p 
 - import pandas as pd
 - from peakdet import peakdet
##Assumptions
- Since I put the peakdet code in its own file that might be an issue for others to run the code.
- You have to close each graph before the next one will show up
- Plots do not save as a file 
##Instructions
- If all functions are available in your terminal you can use: python Peakdetection.py 
##Expected Outcome
- This will show you 3 different scatter plots with the highest peaks shown as red points
- The code in Peakdetection.py will plot data from Option 1 and find the peaks and show the graphs.
 





When to use the code.
To use the graphing and peak detection you will need to have 2 columns of 
data in a libre office file.  The data being pulled will start with the 3rd line(Our 1st line a title and 2nd line was a subtitle of the columnd data)
Your peaks will need to be above 10,000 on the y-axis.

What the code is saying
we first had to import sys, numpy, glob, matplotlib.pyplot, and pandas.  We thendefined peakdetection.
begins with fail safes for the code that's being graphed.
if there are a different amount of values that the amount of "x"s then there
 will be an error.
says if the number is not a scalar than there will be an error.
says if the delta is less than zero than there is an error.
 explains how to find the peaks and if one value is larger than the 
one to the left and right, than it's a peak.
explains defines the excel sheet and names it.  
we made a "for" loop that has the graphing program cycle through all 3 excel
 sheets.
We than ran a an "if" statement to plot the peaks in a "for" loop to go
through all three data sheets in libre office.
 
Nathan- The first major problem for this code was turning it in to a matrix of a graph. Crystal's code was able to translate the excel file in to a graph which could then be used for finding the maxima.

Nathan- importing numpy as np saves time when writng the code for series analysis�	Contains a `README.md� file that summarizes the code in the project, and instructions for how someone can use it. Also, explain any major assumptions that your code relies about the input data, so users know when they can use your code, and when they shouldn�t. Include in the readme the expected output for one invocation of your code so a new user can compare their output to see if it works for them.

Nathan- Pandas works to process the images/graphs

Nathan- Using Matplot.lib is an easy way to get a package that has a peak detection function if you do not have anything else.
