#Peak Detection Code 
#The following seven lines of code are the packages that will allow the program to function and us the data to produce results.
import sys
import numpy
from numpy import NaN, Inf, arange, isscalar, asarray, array
import glob
import matplotlib.pyplot as p
import pandas as pd
from peakdet import peakdet

#these next couple of lines define what the variables are and where the data will be pulled from.
excel_file = 'P3data.xlsx'
Sheet_name = 'Problem1a'

Project3 = pd.ExcelFile('P3data.xlsx')
print(Project3.sheet_names)
for Sheet in [u'Problem1a', u'Problem1b', u'Problem2']:
        if __name__=="__main__":
                from matplotlib.pyplot import plot, scatter, show
                df = Project3.parse(Sheet, skiprows=1)
                y = df.iloc[:,1]
                p.xlabel('Binding Energy (eV)')
                p.ylabel('Intensity (counts/second)')
                print(y.head())
#this will print the whole graph and data.
                maxtab, mintab = peakdet(y,10000)
                plot(y)
                scatter(array(mintab)[:,0], array(mintab)[:,1], color='blue')
                scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='red')
                show()
                p.savefig(Sheet)
                p.clf()
