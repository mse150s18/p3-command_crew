import sys
import numpy
from numpy import NaN, Inf, arange, isscalar, asarray, array
import glob
import matplotlib.pyplot as p
import pandas as pd
from peakdet import peakdet
from scipy.optimize import curve_fit


excel_file = 'P3data.xlsx'
Sheet_name = 'Problem1a'
xdata = 'Binding energy (eV)'
ydata = 'Intensity (counts/second)'


Project3 = pd.ExcelFile('P3data.xlsx')
print(Project3.sheet_names)
for Sheet in [u'Problem1a', u'Problem1b', u'Problem2']:
        if __name__=="__main__":
                from matplotlib.pyplot import plot, scatter, show
                df = Project3.parse(Sheet, skiprows=1)
                y = df.iloc[:,1]
                print(y.head())

                def func(x,m,b):
                    return m*x+b
                params= curve_fit(func,xdata,ydata)
                m,b=params[0]
                print("m-",m, "b-",b)
                
                maxtab, mintab = peakdet(y,10000)
                plot(y)
                scatter(array(mintab)[:,0], array(mintab)[:,1], color='blue')
                scatter(array(maxtab)[:,0], array(maxtab)[:,1], color='red')
                show()
#the code thagt was just used gives the graph the solid red coloring of the peaks.

#Project3 = pd.ExcelFile('P3data.xlsx')
#print(Project3.sheet_names)    
#for Sheet in [u'Problem1a', u'Problem1b', u'Problem2']:
#        if __name__=="__main__":
#                from scipy.optimize import curve_fit
#                df = Project3.parse(Sheet, skiprows=1) 
#                y = df.iloc[:,1]
#print(y.head())
        
#                x = numpy.linspace(y.all())
#                y = func(x.all())
#                noise = numpy.random.normal(0,2, len(y))
#Project3_data = x*3 + 5 + noise
#                def func(x, m, b):
#                    return m*x + b

#                params = curve_fit(func, x, Project3)
#                m, b = params[0]
#                print("m=", m, "b=", b)

#                p.plot(y, m*x + b, label="fit")
#                p.plot(x, Sheet, label="Sheet", alpha=0.5)
#                p.legend()
#                p.savefig("Project3.png")
